(function ($) {
  $(window).ready(function () {

    // Muestra icono si el usuario no está online en Drupal
    var adminpath = window.location.protocol + "//" + window.location.host + "/admin";
    $.ajax({
      url: adminpath,
      success: function () {
        //alert('success');
        $('.footer .drupal-login').hide();
      },
      error: function () {
        //alert('error');
      }
    });

    // Mantiene activo el elemento del menú padre en subrutas
    var path = window.location.pathname.split('/');
    if (path.length > 3) {
      if (path[2] == 'dataset') {
        $("ul.subnav-datos li > a:contains('Conjuntos de datos')").parent().addClass('active');
      } else if (path[2] == 'organization') {
        $("ul.subnav-datos li > a:contains('Organizaciones')").parent().addClass('active');
      } else if (path[2] == 'group') {
        $("ul.subnav-datos li > a:contains('Grupos')").parent().addClass('active');
      }
    }

		// Elimina la cabecera de la tabla 'Acerca de este recurso' si ya se ha definido el 'Mostrar más'
		$(window).load(function () {
				var i = setInterval(function () {
					if ($('.table-resource tr.toggle-show-more').length) {
						clearInterval(i);
						$(".table-resource thead").remove();
					}
				}, 100);
			});
	
  });
})(jQuery);



