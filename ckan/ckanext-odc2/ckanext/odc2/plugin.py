import ckan.plugins as plugins
import ckan.plugins.toolkit as tk
import pylons.i18n

def organization_list():
  '''Devuelve una lista con todas las organizaciones.'''
  return tk.get_action('organization_list')(data_dict={'all_fields': True})


def popular_datasets(limit=5):
  '''Devuelve una lista con los datasets mas populares (los mas visitados).'''
  response = tk.get_action('package_search')(
    data_dict={'sort': 'views_total desc', 'rows': limit}
  )
  return response['results']


def latest_datasets(limit=5):
  '''Devuelve una lista con los ultimos datasets anadidos.'''
  response = tk.get_action('package_search')(
    data_dict={'sort': 'metadata_modified desc', 'rows': limit}
  )
  return response['results']


class Odc2Plugin(plugins.SingletonPlugin):
  plugins.implements(plugins.IConfigurer)
  plugins.implements(plugins.ITemplateHelpers)

  def update_config(self, config):

    # Anadimos el directorio templates de esta extension a CKAN.
    tk.add_template_directory(config, 'templates')

    # Anadimos el directorio public de esta extension a CKAN.
    tk.add_public_directory(config, 'public')

    # Anadimos el directorio fanstatic de esta extension a CKAN.
    tk.add_resource('fanstatic_library', 'ckanext-odc2')

    # Establecemos un titulo y un favicon personalizados en CKAN.
    config['ckan.site_title'] = 'Open Data Canarias'
    config['ckan.favicon'] = '/datos/images/favicon.ico'

  def get_helpers(self):
    return {
      'organization_list': organization_list,
      'popular_datasets': popular_datasets,
      'latest_datasets': latest_datasets,
    }

