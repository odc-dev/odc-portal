from setuptools import setup, find_packages
import sys, os

version = '0.1'

setup(
  name='ckanext-odc2',
  version=version,
  description="Personalizacion de CKAN 2.2 para Open Data Canarias 2",
  long_description='''
  ''',
  classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
  keywords='',
  author='Luis Alberto Rubio Rodriguez',
  author_email='lubertorubior@gmail.com',
  url='',
  license='',
  packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
  namespace_packages=['ckanext', 'ckanext.odc2'],
  include_package_data=True,
  zip_safe=False,
  install_requires=[
    # -*- Extra requirements: -*-
  ],
  entry_points='''
    [ckan.plugins]
    # Add plugins here, e.g.
    # myplugin=ckanext.odc2.plugin:PluginClass
    odc2=ckanext.odc2.plugin:Odc2Plugin
    ''',
)
