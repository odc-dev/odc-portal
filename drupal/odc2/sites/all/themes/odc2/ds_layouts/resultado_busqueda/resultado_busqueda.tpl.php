<?php

/**
 * @file
 * Plantilla Display Suite para un resultado de b�squeda (con envoltorio).
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="ds-resultado-busqueda <?php print $classes;?> clearfix">
  <div class="inner">
  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

    <div class="ds-cabecera">
      <?php print $ds_cabecera ?>
    </div>
    <div class="ds-cuerpo">
      <?php print $ds_cuerpo ?>
    </div>
  </div>
</<?php print $layout_wrapper ?>>


<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
