<?php
function ds_resultado_busqueda() {
  return array(
    'label' => t('Resultado de la búsqueda'),
    'regions' => array(
      'ds_cabecera' => t('Cabecera'),
      'ds_cuerpo' => t('Contenido'),
    ),
    // Add this line if there is a default css file.
    'css' => TRUE,
    // Add this line if you're using DS 2.x for icon preview
    'image' => TRUE,
  );
}
?>
