<?php

/**
 * @file
 * Plantilla Display Suite para un comentario (con envoltorio).
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="ds-comentario <?php print $classes;?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <<?php print $ds_cuerpo_wrapper ?> class="<?php print trim($ds_cuerpo_classes); ?>">
    <div class="ds-cabecera">
        <?php print $ds_cabecera; ?>
    </div>
    <div class="ds-body">
        <?php print $ds_cuerpo; ?>
    </div>
    <<?php print $ds_pie_wrapper ?> class="ds-pie <?php print $ds_pie_classes; ?>">
       <?php print $ds_pie; ?>
    </<?php print $ds_pie_wrapper ?>>
  </<?php print $ds_cuerpo_wrapper ?>>

</<?php print $layout_wrapper ?>>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
