<?php
function ds_comentario() {
  return array(
    'label' => t('Comentario'),
    'regions' => array(
      'ds_cabecera' => t('Cabecera'),
      'ds_cuerpo' => t('Cuerpo'),
      'ds_pie' => t('Pie'),
    ),
    // Add this line if there is a default css file.
    'css' => TRUE,
    // Add this line if you're using DS 2.x for icon preview
    'image' => TRUE,
  );
}
?>
