<?php
function ds_contenido() {
  return array(
    'label' => t('Contenido completo'),
    'regions' => array(
      'ds_titulo' => t('Título'),
      'ds_cabecera_izq' => t('Cabecera (izquierda)'),
      'ds_cabecera_dcha' => t('Cabecera (derecha)'),
      'ds_contenido' => t('Contenido'),
      'ds_pie' => t('Pie'),
    ),
    // Add this line if there is a default css file.
    'css' => TRUE,
    // Add this line if you're using DS 2.x for icon preview
    'image' => TRUE,
  );
}
?>
