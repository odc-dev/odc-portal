<?php

/**
 * @file
 * Plantilla Display Suite para un contenido en modo completo (con envoltorio).
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="ds-contenido <?php print $classes;?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <<?php print $ds_contenido_wrapper ?> class="<?php print trim($ds_contenido_classes); ?>">
    <?php print $ds_titulo ?>
    <div class="ds-cabecera">
      <div class="ds-izq">
       <div class="ver-center">
        <?php print $ds_cabecera_izq ?>
       </div>
      </div>
      <div class="ds-dcha">
       <?php print $ds_cabecera_dcha ?>
      </div>
    </div>
    <?php print $ds_contenido; ?>
  </<?php print $ds_contenido_wrapper ?>>

</<?php print $layout_wrapper ?>>

<<?php print $ds_pie_wrapper ?> class="ds-pie <?php print $ds_pie_classes; ?>">
  <?php print $ds_pie; ?>
</<?php print $ds_pie_wrapper ?>>


<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
