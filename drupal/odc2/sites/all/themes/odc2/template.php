<?php

/**
 * @file
 * template.php
 */

/**
 * Overrides bootstrap_textfield(): preprocesa la entrada de texto.
 */
function odc2_textfield($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'text';
  element_set_attributes($element, array(
    'id',
    'name',
    'value',
    'size',
    'maxlength'
  ));
  _form_set_class($element, array(
    'form-text'
  ));
  
  /* Añade iconos de usuario e email */
  $output = '';
  
  $current_path = $_SERVER['REQUEST_URI'];
  $elem = array(
    'name',
    'mail'
  );
  if (in_array($element['#name'], $elem) && strpos($current_path, "user")) {
    switch ($element['#name']) {
      case 'name':
        $output .= '<div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span></span><input' . drupal_attributes($element['#attributes']) . ' /></div>';
        break;
      case 'mail':
        $output .= '<div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope"></i></span></span><input' . drupal_attributes($element['#attributes']) . ' /></div>';
        break;
    }
  } else {
    $output = '<input' . drupal_attributes($element['#attributes']) . ' />';
  }
  
  //$output = '<input' . drupal_attributes($element['#attributes']) . ' />';
  
  $extra = '';
  if ($element['#autocomplete_path'] && drupal_valid_path($element['#autocomplete_path'])) {
    drupal_add_library('system', 'drupal.autocomplete');
    $element['#attributes']['class'][] = 'form-autocomplete';
    
    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#attributes']['id'] . '-autocomplete';
    $attributes['value'] = url($element['#autocomplete_path'], array(
      'absolute' => TRUE
    ));
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'autocomplete';
    // Uses icon for autocomplete "throbber".
    if ($icon = _bootstrap_icon('refresh')) {
      $output = '<div class="input-group">' . $output . '<span class="input-group-addon">' . $icon . '</span></div>';
    }
    // Fallback to using core's throbber.
    else {
      $output = '<div class="input-group">' . $output . '<span class="input-group-addon">';
      // The throbber's background image must be set here because sites may not
      // be at the root of the domain (ie: /) and this value cannot be set via
      // CSS.
      $output .= '<span class="autocomplete-throbber" style="background-image:url(' . url('misc/throbber.gif') . ')"></span>';
      $output .= '</span></div>';
    }
    $extra = '<input' . drupal_attributes($attributes) . ' />';
  }
  
  return $output . $extra;
}

/**
 * Overrides theme_password(): preprocesa la entrada de contraseñas.
 */
function odc2_password($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'password';
  element_set_attributes($element, array(
    'id',
    'name',
    'size',
    'maxlength'
  ));
  _form_set_class($element, array(
    'form-text'
  ));
  
  /* Añade icono de candado */
  if ($element['#name'] == 'pass') {
    return '<div class="input-group"><span class="input-group-addon"><i class="fa fa-lock"></i></span></span><input' . drupal_attributes($element['#attributes']) . ' /></div>';
  }
  
  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}


/**
 * Implements hook_preprocess_comment(): preprocesa un comentario.
 */
function odc2_preprocess_comment(&$variables) {
  //dpm($variables);
  $variables['submitted'] = '<span class="comment-submitted">' . $variables['submitted'] . '</span>';
  
  // Añade icono al botón de borrar comentario
  if (isset($variables['content']['links']['comment']['#links']['comment-delete'])) {
    $variables['content']['links']['comment']['#links']['comment-delete']['title'] = '<i class="fa fa-times"></i> ' . $variables['content']['links']['comment']['#links']['comment-delete']['title'];
    $variables['content']['links']['comment']['#links']['comment-delete']['attributes']['class'][] = 'btn';
    $variables['content']['links']['comment']['#links']['comment-delete']['attributes']['class'][] = 'btn-default';
  }
  
  // Añade icono al botón de editar comentario
  if (isset($variables['content']['links']['comment']['#links']['comment-edit'])) {
    $variables['content']['links']['comment']['#links']['comment-edit']['title'] = '<i class="fa fa-edit"></i> ' . $variables['content']['links']['comment']['#links']['comment-edit']['title'];
    $variables['content']['links']['comment']['#links']['comment-edit']['attributes']['class'][] = 'btn';
    $variables['content']['links']['comment']['#links']['comment-edit']['attributes']['class'][] = 'btn-default';
  }
  
  // Añade icono al botón de responder comentario
  if (isset($variables['content']['links']['comment']['#links']['comment-reply'])) {
    $variables['content']['links']['comment']['#links']['comment-reply']['title'] = '<i class="fa fa-reply"></i> ' . $variables['content']['links']['comment']['#links']['comment-reply']['title'];
    $variables['content']['links']['comment']['#links']['comment-reply']['attributes']['class'][] = 'btn';
    $variables['content']['links']['comment']['#links']['comment-reply']['attributes']['class'][] = 'btn-default';
  }
  
}

/**
 * Implements hook_preprocess_node(): preprocesa un nodo.
 */
function odc2_preprocess_node(&$variables) {
  $node = $variables['node'];
  // dpm($node);
  
  $variables['tipo_de_contenido'] = '<span class="tipo-de-contenido"><i class="fa fa-folder" title="Tipo de contenido"></i>&nbsp;' . node_type_get_name($variables['node']->type) . '</span>';
  $contenidos = array(
    'sugerencia',
    'aplicacion',
    'blog'
  );
  if (in_array($variables['type'], $contenidos)) {
    
		// Cuenta de comentarios
	  if (isset($node->comment_count)) {
      $cuenta = ($node->comment_count == 1 ? " comentario" : " comentarios");
			$num_coment = $node->comment_count;
		}
		else {
			$cuenta = " comentarios";
			$num_coment = 0;
		}
    $variables['cuenta_comentarios'] = '<span class="cuenta-comentarios"><i class="fa fa-comments" title="Numero de comentarios"></i>&nbsp;<a href="#comments">' . $num_coment . '</a>' . $cuenta . '</span>';
    
		// Menú para compartir en redes sociales
    $path = isset($_GET['q']) ? $_GET['q'] : '<front>';
    $link = url($path, array(
      'absolute' => TRUE
    ));
    
    $fb_share_url = 'https://www.facebook.com/sharer.php?u=' . $link;
    $facebook = l('<i class="fa fa-facebook fa-lg fb"></i>', $fb_share_url, array(
      'html' => TRUE,
      'attributes' => array(
        'title' => 'En Facebook',
        'target' => '_blank',
        'class' => array(
          'btn',
          'btn-default',
          'btn-xs'
        )
      )
    ));
    
    $tw_share_url = 'https://twitter.com/share?url=' . $link;
    $twitter = l('<i class="fa fa-twitter fa-lg tw"></i>', $tw_share_url, array(
      'html' => TRUE,
      'attributes' => array(
        'title' => 'En Twitter',
        'target' => '_blank',
        'class' => array(
          'btn',
          'btn-default',
          'btn-xs'
        )
      )
    ));
    
    $gp_share_url = 'https://plus.google.com/share?url=' . $link;
    $google = l('<i class="fa fa-google-plus fa-lg gp"></i>', $gp_share_url, array(
      'html' => TRUE,
      'attributes' => array(
        'title' => 'En Google+',
        'target' => '_blank',
        'class' => array(
          'btn',
          'btn-default',
          'btn-xs'
        )
      )
    ));
    
    $variables['compartir'] = '<div class="compartir-contenido btn-group"><button class="btn btn-default btn-xs disabled">Compartir:</button>' . $facebook . $twitter . $google . '</div>';
    
    //dpm($variables['content']);
		// Añade icono y convierte en botón el enlace para añadir comentario
    if ($variables['node']->comment == COMMENT_NODE_OPEN) {
      if (user_access('post comments')) {   
        if (isset($variables['content']['links']['comment']['#links']['comment-add']['title'])) {
          $variables['content']['links']['comment']['#links']['comment-add']['html'] = TRUE;
          $variables['content']['links']['comment']['#links']['comment-add']['title'] = '<button class="btn btn-primary"><i class="fa fa-comment"></i> ' . $variables['content']['links']['comment']['#links']['comment-add']['title'] . '</button>';
        }
      }
    } 
    
  }
  
}

/**
 * Implements hook_preprocess_breadcrumb(): preprocesa las breadcrumbs (ayudas para la navegación).
 */
function odc2_preprocess_breadcrumb(&$variables) {
  $breadcrumb =& $variables['breadcrumb'];
  
  if ((theme_get_setting('bootstrap_breadcrumb_home')) && (theme_get_setting('bootstrap_breadcrumb_title')) && !empty($breadcrumb)) {
    $breadcrumb['0'] = l('<i class="fa fa-home"></i>', '<front>', array(
      'html' => TRUE
    ));
  }
  
  if (theme_get_setting('bootstrap_breadcrumb_title') && !empty($breadcrumb)) {
    $node = menu_get_object();
    if ($node) {
      $breadcrumb[count($breadcrumb) - 1] = array(
        'data' => $node->title,
        'class' => array(
          'active'
        )
      );
    }
  }
}

/**
 * Implements hook_preprocess_page(): preprocesa una página.
 */
function odc2_preprocess_page(&$variables) {
  //dpm($variables);
	
  // Errores 404 y 403                    
  $status = drupal_get_http_header("status");
  if ($status == "404 Not Found") {
    //$variables['theme_hook_suggestions'][] = 'page__404';
    $variables['page']['content']['system_main']['main']['#markup'] .= ' <i class="fa fa-envelope-square"></i> Contacto: info@opendatacanarias.es';       
  }            
  if ($status == "403 Forbidden") {
    //$variables['theme_hook_suggestions'][] = 'page__403';
    $variables['page']['content']['system_main']['main']['#markup'] .= ' <i class="fa fa-envelope-square"></i> Contacto: info@opendatacanarias.es';
  }
  
  // Barra de búsqueda
  $search_form = drupal_get_form('search_form');
  $search_box = drupal_render($search_form);
  $variables['search_box'] = '<div class="nav-search">' . $search_box . '</div>';
  
  // Menú footer (columna 2)
  $menu_footer_col2 = menu_tree(variable_get('menu_main_links_source', 'menu-footer-col2'));
  $menu_footer_col2['#theme_wrappers'] = array(
    'menu_tree__footer'
  );
  $variables['menu_footer_col2'] = render($menu_footer_col2);
  
  // Menú footer (columna 3)
  $menu_footer_col3 = menu_tree(variable_get('menu_main_links_source', 'menu-footer-col3'));
  $menu_footer_col3['#theme_wrappers'] = array(
    'menu_tree__footer'
  );
  $variables['menu_footer_col3'] = render($menu_footer_col3);
  
  // Menú footer (columna 4)
  $menu_footer_col4 = menu_tree(variable_get('menu_main_links_source', 'menu-footer-col4'));
  $menu_footer_col4['#theme_wrappers'] = array(
    'menu_tree__footer'
  );
  $variables['menu_footer_col4'] = render($menu_footer_col4);
  
  // Menú footer (columna 5)
  $menu_footer_col5 = menu_tree(variable_get('menu_main_links_source', 'menu-footer-col5'));
  $menu_footer_col5['#theme_wrappers'] = array(
    'menu_tree__footer'
  );
  $variables['menu_footer_col5'] = render($menu_footer_col5);
  
  // Si el usuario no ha iniciado sesión en Drupal, mouestra botón con icono de usuario
  if (user_is_anonymous()) {
    
    $drupal_status = l('<i class="fa fa-lock"></i>', 'user', array(
      'attributes' => array(
        'title' => 'Administracion'
      ),
      'html' => TRUE
    ));
    $variables['menu_login'] = '<span class="drupal-login">' . $drupal_status . '</span>';
    
  }
  
  // Obtiene los submenús
  $variables['submenus'] = _odc2_get_submenus();
  
  // Si el usuario no ha iniciado sesión en CKAN, muestra botón para iniciar sesión
  if (!isset($_COOKIE['auth_tkt'])) {
    
    $login_button = l('<span><i class="fa fa-user"></i></span>', 'datos/user/login', array(
      'attributes' => array(
        'class' => array(
          'login-btn',
          'btn-default',
          'btn',
          'btn-primary'
        )
      ),
      'html' => TRUE
    ));
    $variables['login_button'] = $login_button;
    
  }
  // Si el usuario ha iniciado sesión en CKAN, muestra el menú de usuario
  else {
    
    $login_button = l('<span><i class="fa fa-user"></i></span>', 'datos/user/login', array(
      'attributes' => array(
        'class' => array(
          'login-btn',
          'btn-default',
          'btn',
          'btn-primary'
        ),
        'style' => array(
          'visibility: hidden'
        )
      ),
      'html' => TRUE
    ));
    $variables['login_button'] = $login_button;
    
    $cookie = explode("!", substr($_COOKIE['auth_tkt'], 41));
    $uid_ckan = $cookie['0'];
    
    $base_url = variable_get('ckan_url', 'http://localhost');
    $url_api = $base_url . '/api/3/action/user_show';
    $headers = array(
      // API-key del sysadmin de CKAN 'Open Data Canarias'
      'Authorization' => variable_get('ckan_sysadmin_api_key', '0123'),
      'Content-Type' => 'application/json; charset=utf-8'
    );
    $param = array(
      'id' => $uid_ckan
    );
    $data = json_encode($param);
    $options = array(
      'method' => 'POST',
      'headers' => $headers,
      'data' => $data
    );
    $result = drupal_http_request($url_api, $options);
    if ($result->code == '200') {
      $user_ckan = json_decode($result->data);
      if ($user_ckan->success) {
        //dpm($user_ckan);
        
        $gravatar_default = variable_get('ckan_gravatar_default', 'identicon');
        $user_avatar = 'http://gravatar.com/avatar/' . $user_ckan->result->email_hash . '?s=22&d=' . $gravatar_default;
        $user_rname = $user_ckan->result->display_name;
        $user_uname = $user_ckan->result->name;
        
        $url_api = $base_url . '/api/3/action/dashboard_new_activities_count';
        $headers = array(
          // API-key del usuario actual 
          'Authorization' => $user_ckan->result->apikey
        );
        $options = array(
          'headers' => $headers
        );
        $new_result = drupal_http_request($url_api, $options);
        if ($new_result->code == '200') {
          $new_acts = json_decode($new_result->data);
          if ($new_acts->success) {
            //dpm($new_acts);
            
            $avatar = l('<img src="' . $user_avatar . '" /><span class="username">' . $user_rname . '</span>', 'datos/user/' . $user_uname, array(
              'html' => TRUE,
              'attributes' => array(
                'title' => 'Ver perfil',
                'class' => 'image'
              )
            ));
            $dashboard = l('<i class="fa fa-dashboard"></i><span>' . $new_acts->result . '</span>', 'datos/dashboard', array(
              'html' => TRUE,
              'attributes' => array(
                'title' => 'Panel de Control (' . $new_acts->result . ($new_acts->result != 1 ? ' nuevos elementos)' : ' nuevo elemento)')
              )
            ));
            $notifications_classes = array(
              'notifications'
            );
            if ($new_acts->result > 0) {
              $notifications_classes[] = 'notifications-important';
            }
            
            
            $edit = l('<i class="fa fa-cog"></i>', 'datos/user/edit/' . $user_uname, array(
              'html' => TRUE,
              'attributes' => array(
                'title' => 'Editar opciones'
              )
            ));
            $logout = l('<i class="fa fa-sign-out"></i>', 'datos/user/_logout', array(
              'html' => TRUE,
              'attributes' => array(
                'title' => 'Salir'
              )
            ));
            
            $elementos_menu = array(
              array(
                'data' => $avatar
              ),
              array(
                'data' => $dashboard,
                'class' => $notifications_classes
              ),
              array(
                'data' => $edit
              ),
              array(
                'data' => $logout
              )
            );
            
            if ($user_ckan->result->sysadmin) {
              $admin = l('<i class="fa fa-legal"></i>', 'datos/ckan-admin', array(
                'html' => TRUE,
                'attributes' => array(
                  'title' => 'Opciones de Administrador'
                )
              ));
              $elementos_menu = array(
                'data' => $admin
              ) + $elementos_menu;
            }
            
            $classes = array(
              'unstyled'
            );
            $menu_usuario = theme('item_list', array(
              'items' => $elementos_menu,
              'attributes' => array(
                'class' => $classes
              )
            ));
            //dpm($menu_usuario);
            $variables['menu_usuario'] = $menu_usuario;
            
          }
        }
      }
    }
    /**/
    
  }
  
}

/**
 * Funciones para envolver los elementos del menú principal y del footer, respectivamente
 */
function odc2_menu_tree__primary(&$variables) {
  return '<ul class="text-links nav navbar-nav">' . $variables['tree'] . '</ul>';
}

function odc2_menu_tree__footer(&$variables) {
  return '<ul class="footer-links">' . $variables['tree'] . '</ul>';
}

/**
* Overrides theme_menu_link(): muestra un elemento del menú
 */
function odc2_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
  //dpm($element);
  
  // Elemento del menú principal
  if ($element['#original_link']['menu_name'] == 'main-menu') {
    
    $element['#localized_options']['attributes']['class'][] = 'trigger-subnav';
    //dpm($element);
    $ruta_activa = '';
    $active_trail = menu_get_active_trail();
    
    foreach ($active_trail as $trail => $at) {
      if (isset($active_trail[$trail]['menu_name'])) {
        if ($active_trail[$trail]['menu_name'] != 'main-menu') {
          $ruta_activa = $active_trail[$trail]['link_path'];
          break;
        }
      }
    }
    //dpm($element);
    //dpm($href);
    if ($ruta_activa != '') {
      
      if ($element['#original_link']['link_path'] == $ruta_activa) {
        $element['#localized_options']['attributes']['class'][] = 'active';
        $element['#attributes']['class'][] = 'active';
      }
      
      else if ($element['#below']) {
        
        foreach ($element['#below'] as $hijo => $h) {
          if (isset($element['#below'][$hijo]['#original_link'])) {
            if ($element['#below'][$hijo]['#original_link']['link_path'] == $ruta_activa) {
              $element['#localized_options']['attributes']['class'][] = 'active';
              $element['#attributes']['class'][] = 'active';
            }
          }
        }
        
      }
    }
    
	// Tiene hijos
    if ($element['#below']) {
      $element['#title'] .= ' <span class="caret"></span>';
      $element['#localized_options']['html'] = TRUE;
    }
    
  }
  
  // Elemento que no es del menú principal
  else {
    
    if ($element['#below']) {
      // Prevent dropdown functions from being added to management menu so it
      // does not affect the navbar module.
      if (($element['#original_link']['menu_name'] == 'management') && (module_exists('navbar'))) {
        $sub_menu = drupal_render($element['#below']);
      } elseif ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] == 1)) {
        // Add our own wrapper.
        unset($element['#below']['#theme_wrappers']);
        $sub_menu = '<ul class="dropdown-menu">' . drupal_render($element['#below']) . '</ul>';
        // Generate as standard dropdown.
        $element['#title'] .= ' <span class="caret"></span>';
        $element['#attributes']['class'][] = 'dropdown';
        $element['#localized_options']['html'] = TRUE;
        
        // Set dropdown trigger element to # to prevent inadvertant page loading
        // when a submenu link is clicked.
        $element['#localized_options']['attributes']['data-target'] = '#';
        $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
        $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
      }
    }
    
  }
  // On primary navigation menu, class 'active' is not set on active menu item.
  // @see https://drupal.org/node/1896674
  if (($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page())) && (empty($element['#localized_options']['language']))) {
    $element['#attributes']['class'][] = 'active';
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);

  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Muestra los submenús del menú principal
 */
function _odc2_get_submenus() {
  $menu_output = '';
  $main_menu = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
  
  //dpm($main_menu);
  
  foreach ($main_menu as $menu => $m) {
    if (isset($main_menu[$menu]['#title'])) {
      if ($main_menu[$menu]['#below']) {
        
        $classes = array();
        $elemento_menu = array();
        
        $title_saneado = str_replace(' ', '-', strtolower(transliteration_get($main_menu[$menu]['#original_link']['link_title'])));
        $classes[] = 'subnav';
        $classes[] = 'subnav-' . $title_saneado;
        
        foreach ($main_menu[$menu]['#below'] as $submenu => $sm) {
          
          if (isset($sm['#title'])) {
            // Requiere el modulo Transliteration
            $i = 'menu-' . str_replace(' ', '-', strtolower(transliteration_get($sm['#title'])));
            
            $elemento_menu[$i] = array(
              'title' => $sm['#title'],
              'href' => $sm['#href']
            );
            $href = explode("/", $sm['#href']);
            
            $current_path = $_SERVER['REQUEST_URI'];
            if (strpos($current_path, $href[0]) == 1) {
              $classes[] = 'active';
              $elemento_menu[$i]['attributes']['class'][] = 'active';
            }
            
          }
          
        }
        
        $menu_output .= theme('links__menu-' . $title_saneado, array(
          'links' => $elemento_menu,
          'attributes' => array(
            'class' => $classes
          )
        ));
        
        //dpm($menu_output);
      }
    }
  }
  return $menu_output;
}


/**
 * Función para envolver el formulario de búsqueda
 */
function odc2_bootstrap_search_form_wrapper($variables) {
  $output = '<div class="input-group">';
  $output .= $variables['element']['#children'];
  $output .= '<span class="input-group-btn">';
  $output .= '<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>';
  $output .= '</span>';
  $output .= '</div>';
  return $output;
}


/**
 * Implements hook_preprocess_block(): preprocesa un bloque.
 */
function odc2_preprocess_block(&$variables) {
  //dpm($variables);
  if (!empty($variables['block']->subject)) {
    $variables['classes_array'][] = 'block-odc2';
    $variables['classes_array'][] = 'block-odc2-default';
  }
}

/**
 * Capturas de pantalla de una aplicación
 */
function odc2_field__field_capturas__aplicacion($variables) {
  $output = '';
  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    //$output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . ':&nbsp;</div>';
    $output .= '<h2 class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</h2>';
  }
  //dpm($variables);
  // Render the items.
  if ($variables['element']['#view_mode'] != 'search_result') {
    $output .= '<div class="field-items well"' . $variables['content_attributes'] . ' style="text-align: center;">';
  } else {
    $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
  }
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . ' style="display: inline-block;">' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';
  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';
  return $output;
}

/**
 * Vídeo de una aplicación
 */
function odc2_field__field_video__aplicacion($variables) {
  $output = '';
  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h2 class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</h2>';
  }
  // Render the items.
  $output .= '<div class="field-items well"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';
  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';
  return $output;
}

/**
 * Etiquetas.
 */
function odc2_field__field_tags($variables) {
  $output = '';
  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h2 class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</h2>';
  }
  // Render the items.
  $output .= '<ul class="field-items tag-list well"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $item['#attributes']['class'][] = 'tag';
    $output .= '<li class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';
  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';
  return $output;
}

/**
 * Conjuntos de datos que utiliza una aplicación.
 */
function odc2_field__field_datasets_utiliza__aplicacion($variables) {
  $output = '';
  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h2 class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</h2>';
  }
  // Render the items.
  $output .= '<ul class="field-items fa-ul"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<li class="' . $classes . '"' . $variables['item_attributes'][$delta] . '><i class="fa-li fa fa-sitemap" title="Conjunto de datos"></i>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';
  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';
  return $output;
}

/**
 * Estado de una sugerencia.
 */
function odc2_field__field_estado__sugerencia($variables) {
  $output = '';
  //dpm($variables);
  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    if ($variables['element']['#view_mode'] != 'search_result') {
      $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</div>';
    } else {
      $output .= '<div class="field-label"' . $variables['title_attributes'] . '><i class="fa fa-flag" title="Estado de revision"></i></div>';
    }
  }
  // Render the items.
  $output .= '<div class="field-items "' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';
  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';
  return $output;
}

/**
 * Plataformas con las que es compatible una aplicaicón.
 */
function odc2_field__field_plataformas__aplicacion($variables) {
  $output = '';
  //dpm($variables);
  // Render the label, if it's not hidden.
  if ($variables['element']['#view_mode'] == 'search_result') {
    $output .= '<span class="separa-campos">|</span>';
  }
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</div>';
  }
  
  if ($variables['element']['#formatter'] == 'taxonomy_term_reference_plain') {
    foreach ($variables['items'] as $delta => $item) {
      $descripcion = strip_tags($variables['element']['#items'][$delta]['taxonomy_term']->description);
      switch ($item['#markup']) {
        case 'Android':
          $output .= '<span class="item-plataforma"><i class="fa fa-android" title="' . $descripcion . '"></i></span>';
          break;
        case 'iOS':
          $output .= '<span class="item-plataforma"><i class="fa fa-apple" title="' . $descripcion . '"></i></span>';
          break;
        case 'Linux':
          $output .= '<span class="item-plataforma"><i class="fa fa-linux" title="' . $descripcion . '"></i></span>';
          break;
        case 'Navegador web (multiplataforma)':
          $output .= '<span class="item-plataforma"><i class="fa fa-html5" title="' . $descripcion . '"></i></span>';
          break;
        case 'OS X':
          $output .= '<span class="item-plataforma"><i class="fa fa-apple" title="' . $descripcion . '"></i></span>';
          break;
        case 'Windows':
          $output .= '<span class="item-plataforma"><i class="fa fa-windows" title="' . $descripcion . '"></i></span>';
          break;
        case 'Otro':
          $output .= '<span class="item-plataforma"><i class="fa fa-question-circle" title="' . $descripcion . '"></i></span>';
          break;
      }
    }
  } else {
    // Render the items.
    $output .= '<div class="field-items "' . $variables['content_attributes'] . '>';
    foreach ($variables['items'] as $delta => $item) {
      $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
      $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
    }
    $output .= '</div>';
  }
  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';
  return $output;
}

/**
 * Añade un icono al texto de un campo botón (amarillo/azul/blanco).
 */
function _odc2_boton_color_icono($texto) {
  switch ($texto) {
    case 'Sugerencias':
      $icono = '<i class="fa fa-lightbulb-o"></i>';
      break;
    case 'Blog':
      $icono = '<i class="fa fa-rss"></i>';
      break;
    case 'Únete':
      $icono = '<i class="fa fa-thumbs-o-up"></i>';
      break;
    default:
      $icono = _bootstrap_iconize_text($texto);
  }
  return $icono;  
}

/**
 * Botón amarillo de un bloque.
 */
function odc2_field__field_boton_amarillo($variables) {
  $output = '';
  //dpm($variables);
  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h2 class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</h2>';
  }
  // Render the items.
  $output .= '<div class="field-items "' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
	$icono = _odc2_boton_color_icono($item['#element']['title']);
    $item['#element']['title'] = $icono . $item['#element']['title'];
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';
  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';
  return $output;
}

/**
 * Botón azul de un bloque.
 */
function odc2_field__field_boton_azul($variables) {
  $output = '';
  //dpm($variables);
  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h2 class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</h2>';
  }
  // Render the items.
  $output .= '<div class="field-items "' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
	$icono = _odc2_boton_color_icono($item['#element']['title']);    
    $item['#element']['title'] = $icono . $item['#element']['title'];
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';
  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';
  return $output;
}

/**
 * Botón blanco de un bloque.
 */
function odc2_field__field_boton_blanco($variables) {
  $output = '';
  //dpm($variables);
  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h2 class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</h2>';
  }
  // Render the items.
  $output .= '<div class="field-items "' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
	$icono = _odc2_boton_color_icono($item['#element']['title']);
    $item['#element']['title'] = $icono . $item['#element']['title'];
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';
  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';
  return $output;
}

/**
 * Campo (por defecto).
 */
function odc2_field($variables) {
  $output = '';
  //dpm($variables);
  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h2 class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</h2>';
  }
  // Render the items.
  $output .= '<div class="field-items "' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';
  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';
  return $output;
}

/**
 * Título de una aplicación.
 */
function odc2_field__title__aplicacion($variables) {
  $output = '';
  //dpm($variables);
  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h2 class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</h2>';
  }
  
  if ($variables['element']['#view_mode'] == 'full') {
    $logo = '';
    if (isset($variables['element']['#object']->field_icono_app)) {
      $logo_url = $variables['element']['#object']->field_icono_app['und'][0]['uri'];
      $logo = '<span class="logo-app"><img src="' . image_style_url("mini_thumbnail", $logo_url) . '" /></span>';
    }
  }
  //dpm($logo);
  
  // Render the items.
  $output .= '<div class="field-items "' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>';
    if (($delta == 0) && ($variables['element']['#view_mode'] == 'full')) {
      $output .= $logo;
    }
    $output .= drupal_render($item) . '</div>';
  }
  $output .= '</div>';
  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';
  return $output;
}

/**
 * Fecha y autor de la publicacion de un contenido.
 */
function odc2_field__submitted_by($variables) {
  $output = '';
  //dpm($variables);
  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h2 class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</h2>';
  }
  // Render the items.
  $output .= '<div class="field-items "' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    if (strpos($variables['items'][$delta]['#markup'], 'href') === FALSE) {
      $item['#markup'] = str_replace('(no verificado)', '', $item['#markup']);
      $classes = ' anonymous';
    }
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';
  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';
  return $output;
}

/**
 * Fecha en la que se actualizó un contenido.
 */
function odc2_field__changed_date($variables) {
  $output = '';
  //dpm($variables);
  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h2 class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</h2>';
  }
  // Render the items
  
  $variables['items'][0]['#markup'] = 'Actualizado el ' . $variables['items'][0]['#markup'];
  if ($variables['element']['#view_mode'] == 'search_result') {
    $variables['items'][0]['#markup'] = '<span class="separa-campos">|</span>' . $variables['items'][0]['#markup'];
  }
  $output .= '<div class="field-items "' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';
  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';
  return $output;
}

/**
 * Nombre del desarrollador de una aplicación.
 */
function odc2_field__field_desarrollada_por__aplicacion($variables) {
  $output = '';
  //dpm($variables);
  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    /*if ($variables['element']['#view_mode'] != 'search_result') {*/
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</div>';
    /*}
    else {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '</div>';
    }*/
  }
  // Render the items.
  $output .= '<div class="field-items "' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';
  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';
  return $output;
}

/**
 * Valoración de una aplicación, sugerencia o entrada de blog.
 */
function odc2_field__field_valoracion($variables) {
  $output = '';
  //dpm($variables);
  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    if ($variables['element']['#view_mode'] != 'search_result') {
      $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . ':&nbsp;</div>';
    } else {
      $output .= '<div class="field-label"' . $variables['title_attributes'] . '><span class="separa-campos">|</span><i class="fa fa-fire" title="Valoración"></i></div>';
    }
  }
  // Render the items.        
  $output .= '<div class="field-items "' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';
  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';
  return $output;
}

/**
 * Implements hook_preprocess_user_profile_category(): preprocesa el perfil de un usuario.
 */
function odc2_preprocess_user_profile_category(&$variables) {
  //dpm($variables);
  $variables['title'] = '';
}

/**
 * Paginador.
 */
function odc2_pager_link($variables) {
  $text = $variables['text'];
  $page_new = $variables['page_new'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $attributes = $variables['attributes'];
  
  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }
  
  $query = array();
  if (count($parameters)) {
    $query = drupal_get_query_parameters($parameters, array());
  }
  if ($query_pager = pager_get_query_parameters()) {
    $query = array_merge($query, $query_pager);
  }
  
  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
        t('� first') => t('Go to first page'),
        t('. previous') => t('Go to previous page'),
        t('next .') => t('Go to next page'),
        t('last �') => t('Go to last page')
      );
    }
    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    } elseif (is_numeric($text)) {
      $attributes['title'] = t('Go to page @number', array(
        '@number' => $text
      ));
    }
  }
  
  // @todo l() cannot be used here, since it adds an 'active' class based on the
  //   path only (which is always the current path for pager links). Apparently,
  //   none of the pager links is active at any time - but it should still be
  //   possible to use l() here.
  // @see http://drupal.org/node/1410574
  $attributes['href'] = url($_GET['q'], array(
    'query' => $query
  ));
  return '<a' . drupal_attributes($attributes) . '>' . $text . '</a>';
}

/**
 * Devuelve el HTML para un campo imagen formateada con fancyBox.
 *
 * @param $variables array
 */
function odc2_fancybox_imagefield($variables) {
  $image = $variables['image'];
  $path = $variables['path'];
  $caption = $variables['caption'];
  $group = $variables['group'];
  
  return l(theme(empty($image['style_name']) ? 'image' : 'image_style', $image), $path, array(
    'html' => TRUE,
    'attributes' => array(
      'title' => $caption,
      'class' => array(
        'fancybox',
        'thumbnail'
      ),
      'data-fancybox-group' => $group
    )
  ));
}

/**
 * Implements hook_preprocess_html(): preprocesa el Código html del sitio para añadir título a las pestañas del navegador
 * de las páginas que no tienen título visible.
 */
function odc2_preprocess_html(&$variables, $hook) {
  if ($hook == 'html' && empty($variables['title'])) {
    $page = page_manager_get_current_page();
    if ($page) {
      //dpm($page);
      switch ($page['task']['name']) {
        case 'page':
          $titulo = $page['subtask']['subtask']->admin_title;
          break;
        case 'node_view':
          $titulo = $page['contexts']['argument_entity_id:node_1']->data->title;
          break;
        case 'term_view':
          $titulo = $page['contexts']['argument_term_1']->title;
          break;
        case 'user_view':
          $titulo = $page['contexts']['argument_entity_id:user_1']->title;
          break;
        default:
          $titulo = '';
      }
      $head_title = array(
        $titulo,
        variable_get('site_name', 'Drupal')
      );
      $variables['head_title'] = implode(' | ', $head_title);
    }
  }
}

/**
 * Mensaje cuando una búsqueda no obtiene resultados.
 */
function odc2_apachesolr_search_noresults() {
  return t('<ul class="no-results">
<li>Comprueba que has escrito correctamente las palabras de tu búsqueda, o prueba a eliminar algún filtro de la misma.</li>
<li>Elimina de tu búsqueda las comillas que envuelven a las frases para así buscar cada palabra individualmente: <em>"Turismo en Tenerife"</em> tendrá menos resultados que <em>Turismo en Tenerife</em>.</li>
<li>Puedes requerir o excluir términos en la búsqueda usando + y -: <em>Turismo +2013 Tenerife</em> requerirá que los resultados contengan la palabra <em>2013</em>, mientras que <em>Turismo 2013 -Tenerife</em> excluirá los resultados que contengan <em>Tenerife</em>.</li>
</ul>');
}

/**
 * Muestra la vista previa de un nodo.
 */
function odc2_node_preview($variables) {
  $node = $variables['node'];
  $elements = node_view($node, 'full');
  $full = drupal_render($elements);
  $output = '<div class="preview">';
  $output .= '<h3 class="post-preview" >' . t('Vista previa parcial del contenido') . '</h3>';
  $output .= $full;
  $output .= "</div>\n";
  return $output;
}

/**
 * Implements hook_preprocess_image_style(): preprocesa los estilos de imagen.
 */
/*function odc2_preprocess_image_style(&$vars) {
  $vars['attributes']['class'][] = 'img-responsive';
}*/
