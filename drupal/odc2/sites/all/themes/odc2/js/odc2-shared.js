(function ($) {

  $(function () {
    initNav();
  });

  // Menú principal
  function initNav() {
    var triggers = $('.trigger-subnav');
    var bluebar = $('#bluebar');
    window.navOpen = false;
    $('body').addClass('navClosed');
    triggers.click(function (e) {
      var target = $(e.delegateTarget);
      var subnav, propagateEvent;
      var nav_activa_new = $();
      var nav_activa_old = $();
      var new_href = $();

      // Detecta el elemento del menú que está activo
      if ($('.nav-inicio').hasClass('active')) {
        nav_activa_old = $('.nav-inicio');
      } else if ($('.nav-datos').hasClass('active')) {
        nav_activa_old = $('.nav-datos');
      } else if ($('.nav-apps').hasClass('active')) {
        nav_activa_old = $('.nav-apps');
      } else if ($('.nav-participa').hasClass('active')) {
        nav_activa_old = $('.nav-participa');
      }

      // Comportamiento según el menú que se cliquee
      if (target.hasClass('nav-inicio')) {
        subnav = $();
        propagateEvent = true;
      } else if (target.hasClass('nav-datos')) {
        subnav = $('.subnav-datos');
        nav_activa_new = $('.nav-datos');
        datos = true;
        new_href = $('.nav-datos');
      } else if (target.hasClass('nav-apps')) {
        subnav = $();
        propagateEvent = true;
      } else if (target.hasClass('nav-participa')) {
        subnav = $('.subnav-participa');
        nav_activa_new = $('.nav-participa');
      } else {
        throw 'Unrecognised subnav trigger', target;
      }
      var bluebar = $('#bluebar');
      // --
      var old_height = bluebar.height();
      $('body').toggleClass('navOpen', window.navOpen);
      $('body').toggleClass('navClosed', !window.navOpen);

      // Actualiza el elemento activo
      if (subnav != $()) {
        if (nav_activa_new != nav_activa_old) {
          nav_activa_old.removeClass('active');
          nav_activa_old.closest('li').removeClass('active');

          nav_activa_new.addClass('active');
          nav_activa_new.closest('li').addClass('active');
        }
      }

      // Muestra la barra azul con los submenús (bluebar)
      $('.subnav').removeClass('active');
      subnav.addClass('active');
      var new_height = bluebar.outerHeight();
      bluebar.height(old_height);
      bluebar.stop().animate({
        height: new_height
      }, 300, 'swing', function () {
        bluebar.css('height', 'auto');

        if (new_href != $()) {

          $(location).attr('href', new_href.attr('href'));
        }

      });

      if (!propagateEvent) {
        e.preventDefault();
        return false;
      }
    });
  }

})(jQuery);