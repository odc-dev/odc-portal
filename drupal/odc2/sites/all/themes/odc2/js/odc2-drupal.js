(function ($) {

  // Calcula la altura máxima de un elemento y la aplica a todos los elementos comunes
  mismaAltura = function (container) {
    var currentTallest = 0,
      currentRowStart = 0,
      rowDivs = new Array(),
      $el,
      topPosition = 0;
    $(container).each(function () {

      $el = $(this);
      $($el).height('auto')
      topPostion = $el.position().top;

      if (currentRowStart != topPostion) {
        for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
          rowDivs[currentDiv].height(currentTallest);
        }
        rowDivs.length = 0;
        currentRowStart = topPostion;
        currentTallest = $el.height();
        rowDivs.push($el);
      } else {
        rowDivs.push($el);
        currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
      }
      for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
        rowDivs[currentDiv].height(currentTallest);
      }
    });
  }

  $(window).ready(function () {

    // Añade iconos a los fieldsets de los formularios de añadir contenido
    $(".page-node-add .field-group-fieldset .fieldset-title").each(function () {
      $(this.lastChild).wrap('<span class="heading-text"></span>');
      $(this).replaceWith('<span class="fieldset-title"><i class="heading-icon fa fa-arrow-circle-o-down"></i>' + $(this).html() + '</span>');
    });
    $(".page-node-add .field-group-fieldset .panel-heading").click(function () {
      if ($(this).next('.panel-collapse').hasClass('in')) {
        $(this).find('.heading-icon').replaceWith('<i class="heading-icon fa fa-arrow-circle-o-down"></i>');
      } else {
        $(this).find('.heading-icon').replaceWith('<i class="heading-icon fa fa-arrow-circle-o-up"></i>');
      }
    });

    // Si la resolución de visualización es pequeña, cambia 'Aplicaciones' en el menú por 'Apps' (basado en media-queries)
    if ($("#odc2-nav .text-links").css("font-size") <= "14px") {
      $('#odc2-nav .text-links a.nav-apps:contains(Aplicaciones)').html('Apps');
    }

    // Si la resolución de visualización es pequeña, solo muestra el nombre (sin apellidos) en el menú de usuario (basado en media-queries)
    $(".account ul li a span.username").data("nombre-original", $(".account ul li a span.username").text());
    if ($("#odc2-nav .nav-search *").css("visibility") === 'hidden') {
      $(".account ul li a span.username").text(function (index, oldText) {
        var oldName = $(this).data("nombre-original");
        var newName = oldName.split(' ');
        return oldText.replace(oldName, newName[0]);
      });
    }

    // Bloques cliqueables en la página de inicio
    $(".front .block-bean").click(function () {
      window.location = $(this).find(".content .field-type-link-field a").attr("href");
      return false;
    });

  });

  $(window).load(function () {
    // Bloques de la página de inicio con la misma altura
    mismaAltura('.front .block-bean');
  });

  $(window).resize(function () {

    // Si la resolución de visualización es pequeña, cambia 'Aplicaciones' en el menú por 'Apps' (basado en media-queries)
    if ($("#odc2-nav .text-links").css("font-size") <= "14px") {
      $('#odc2-nav .text-links a.nav-apps:contains(Aplicaciones)').html('Apps');
    } else {
      $('#odc2-nav .text-links a.nav-apps:contains(Apps)').html('Aplicaciones');
    }

    // Bloques de la página de inicio con la misma altura
    mismaAltura('.front .block-bean');

    // Si la resolución de visualización es pequeña, solo muestra el nombre (sin apellidos) en el menú de usuario (basado en media-queries)
    if ($("#odc2-nav .nav-search *").css("visibility") === 'hidden') {
      $(".account ul li a span.username").text(function (index, oldText) {
        var oldName = $(this).data("nombre-original");
        var newName = oldName.split(' ');
        return oldText.replace(oldName, newName[0]);
      });
    } else {
      $(".account ul li a span.username").text(function (index, oldText) {
        var oldName = $(this).text();
        var newName = $(this).data("nombre-original");
        return oldText.replace(oldName, newName);
      });
    }

  });

})(jQuery);