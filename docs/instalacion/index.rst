======================
Instalación del Portal
======================

En esta sección se describe los pasos a seguir para realizar la instalación del portal Open Data Canarias desde cero. El proceso parte de un **servidor con el sistema operativo recien instalado**.

Requisitos de la plataforma
---------------------------
El servidor debe tener los siguientes requisitos para que tenga un rendimiento aceptable:

* **Sistema Operativo:** Ubuntu Server 12.04 LTS 64bits
* **Memoria RAM:** 16 Gb RAM
* **Espacio en disco duro:** 1 TeraByte
* **Dominio registrado:** `opendatacanarias.es <http://opendatacanarias.es>`_

Tipos de Instalación
--------------------
Se establecen dos procesos  para realizar la instalación:
 .. toctree::
    :maxdepth: 1

    automatica
    pasoAPaso

El primero realiza la instalación en el menor número de pasos posibles y automatizando muchos de ellos. En cambio, el segundo documenta cada uno de los pasos por si es necesario hacerlos por separado.