======================
Instalación automática
======================

La instalación se realiza automátizando el proceso en la medida de lo posible para que sea más rápido y controlado. RETO: 559

Pasos previos
-------------

#. Antes de comenzar con la instalación es necesario actualizar e instalar algunos paquetes básicos::

    sudo apt-get update
    sudo apt-get dist-upgrade
    sudo apt-get install links2 vim git wget patch

#. Descargar código del portal desde el repositorio git::

    cd ~
    git clone https://bitbucket.org/odc-dev/odc-portal.git
    cd odc-portal
    git tag v0.1

Instalación
-----------

#. Instalación de paquetes::

    sudo apt-get install apache2 nginx mysql-server postgresql-9.1 postgresql-9.1-postgis php5 php5-mysql php5-gd libapache2-mod-wsgi libpq5 python python-dev openjdk-7-jre tomcat7 tomcat7-docs tomcat7-examples tomcat7-admin libxml2-dev libxslt1-dev libgeos-c1 ssmtp

   .. note:: Añadir y guardar la contraseña root a MySQL

    .. parsed-literal::

        Password\: |mysqlROOTPassword|

#. Descargar y descomprmir paquetes::

    cd ~
    wget http://packaging.ckan.org/python-ckan_2.2_amd64.deb
    wget http://archive.apache.org/dist/lucene/solr/4.7.0/solr-4.7.0.tgz
    tar zxvf solr-4.7.0.tgz

#. Instalar paquetes::

    sudo dpkg -i python-ckan_2.2_amd64.deb

#. Crear los directorios::

    sudo mkdir -p /usr/share/solr/ckan /var/lib/solr/data/ckan/index /etc/solr/ckan /usr/share/solr/drupal /var/lib/solr/data/drupal/index /etc/solr/drupal /var/log/solr /var/lib/ckan/default /usr/share/solr/lib

#. Copiar código repositorio y descargados:

   .. parsed-literal::
        sudo cp ~/odc-portal/ckan/etc/production.ini /etc/ckan/default/production.ini
        sudo cp -r ~/odc-portal/ckan/ckanext-odc2 /usr/lib/ckan/default/src/
        sudo cp -r |codePathDrupalCode| |drupalPath|
        sudo cp solr-4.7.0/dist/solr-4.7.0.war /usr/share/solr
        sudo cp -r solr-4.7.0/example/solr/collection1/conf /etc/solr/ckan
        sudo cp -r solr-4.7.0/example/solr/collection1/conf /etc/solr/drupal
        sudo cp solr-4.7.0/dist/* /usr/share/solr/lib
        sudo cp solr-4.7.0/contrib/analysis-extras/lib/* /usr/share/solr/lib
        sudo cp solr-4.7.0/contrib/clustering/lib/* /usr/share/solr/lib
        sudo cp solr-4.7.0/contrib/dataimporthandler/lib/* /usr/share/solr/lib
        sudo cp solr-4.7.0/contrib/extraction/lib/* /usr/share/solr/lib
        sudo cp solr-4.7.0/contrib/langid/lib/* /usr/share/solr/lib
        sudo cp solr-4.7.0/contrib/uima/lib/* /usr/share/solr/lib
        sudo cp solr-4.7.0/contrib/velocity/lib/* /usr/share/solr/lib
        sudo cp solr-4.7.0/dist/solrj-lib/* /usr/share/tomcat7/lib/
        sudo cp solr-4.7.0/example/resources/log4j.properties /usr/share/tomcat7/lib
        sudo ln -s /etc/solr/ckan/conf /usr/share/solr/ckan/conf
        sudo ln -s /etc/solr/drupal/conf /usr/share/solr/drupal/conf
        sudo rm -f /etc/solr/ckan/conf/schema.xml
        sudo ln -s /usr/lib/ckan/default/src/ckan/ckan/config/solr/schema-2.0.xml /etc/solr/ckan/conf/schema.xml
        sudo cp /var/www/odc2-drupal/sites/all/modules/apachesolr/solr-conf/solr-4.x/* /etc/solr/drupal/conf/

#. Cambiar permisos de carpetas:

   .. parsed-literal::

        sudo chown -R www-data:www-data |drupalPath|/sites/default/files
        sudo chown -R tomcat7:tomcat7 /var/log/solr /usr/share/solr /var/lib/solr
        sudo chown www-data /var/lib/ckan/default
        sudo chmod u+rwx /var/lib/ckan/default

#. MySQL crear base de datos:

   .. parsed-literal::

        mysql -u root -p
        CREATE DATABASE |mysqlDRUPALDatabase|;

   Crear el usuario y otorgarle permisos:

   .. parsed-literal::

        CREATE USER '|mysqlDRUPALUser|'@'localhost' IDENTIFIED BY '|mysqlDRUPALPassword|';
        GRANT ALL PRIVILEGES ON |mysqlDRUPALDatabase|.* TO |mysqlDRUPALUser|\@localhost IDENTIFIED BY '|mysqlDRUPALPassword|';
        FLUSH PRIVILEGES;

   .. note:: Guardar los datos de conexión: usuario, contraseña y base de datos.

#. PostgreSQL crear usuarios y bases de datos::

    sudo -u postgres createuser -S -D -R -P ckan_default
    sudo -u postgres createuser -S -D -R -P -l datastore_default
    sudo -u postgres createdb -O ckan_default ckan_default -E utf-8
    sudo -u postgres createdb -O ckan_default datastore_default -E utf-8
    sudo -u postgres psql -d ckan_default -f /usr/share/postgresql/9.1/contrib/postgis-1.5/postgis.sql
    sudo -u postgres psql -d ckan_default -f /usr/share/postgresql/9.1/contrib/postgis-1.5/spatial_ref_sys.sql

   .. note::
        Guardar los datos del usuario.
        ckan_default para CKAN.
        datastore_default para plugin Datastore.

#. cambiar el dueño de las tablas espaciales al usuario de CKAN (ckan_default)::

    sudo -u postgres psql postgres

   Ejecutar en postgres::

    \c ckan_default
    ALTER TABLE spatial_ref_sys OWNER TO ckan_default;
    ALTER TABLE geometry_columns OWNER TO ckan_default;
    \q

#. Restaurar la copia inicial de la base de datos:

   .. parsed-literal::

        mysql -u |mysqlDRUPALUser| -p |mysqlDRUPALDatabase| < |codePathDrupal|/|codeDrupalDump|

   .. note:: Solicita la contraseña para el usuario |mysqlDRUPALUser|

#. Editar fichero de configuración::

    sudo vim /etc/ckan/default/production.ini

   Editar los datos conexión del usuario de ckan (linea 46), (linea 48-49), (linea 86), (linea 125)::

    sqlalchemy.url = postgresql://ckan_default:fooCkanPassword@localhost/ckan_default

    ckan.datastore.write_url = postgresql://ckan_default:fooCkanPassword@localhost/datastore_default
    ckan.datastore.read_url = postgresql://datastore_default:fooDatastorePassword@localhost/datastore_default

    ckan.plugins = stats text_preview recline_preview datastore datapusher resource_proxy pdf_preview spatial_query spatial_metadata geojson_preview wms_preview odc2

    ckan.storage_path = /var/lib/ckan/default

#. Inicializar la base de datos::

    sudo ckan db init

#. Activar el entorno virtual e instalar módulos::

    sudo su
    . /usr/lib/ckan/default/bin/activate
    cd /usr/lib/ckan/default/src/ckan
    paster --plugin=ckan datastore set-permissions postgres -c /etc/ckan/default/production.ini
    paster sysadmin add admin -c /etc/ckan/default/production.ini
    pip install -e git+https://github.com/okfn/ckanext-spatial.git@stable#egg=ckanext-spatial
    cd /usr/lib/ckan/default/src/ckanext-spatial/
    pip install -r pip-requirements.txt
    cd /usr/lib/ckan/default/src/ckanext-odc2/
    python setup.py develop
    exit

   .. note:: Almacenar la contraseña del administrador.

#. Ejecutar patch con cada parche::

    sudo patch -p1 < ~/odc-portal/ckan/patches/traducir_widget_1.patch
    sudo patch -p1 < ~/odc-portal/ckan/patches/traducir_widget_2.patch
    sudo patch -p1 < ~/odc-portal/ckan/patches/redirigir_usuario_home.patch
    sudo patch -p1 < ~/odc-portal/ckan/patches/24_items_per_page.patch
    sudo patch -p1 < ~/odc-portal/ckan/patches/traducir_dataset_extent_1.patch
    sudo patch -p1 < ~/odc-portal/ckan/patches/traducir_dataset_extent_2.patch

#. Consultar la API Key en CKAN del usuario::

    sudo -u postgres -H -- psql -d ckan_default -c "SELECT apikey FROM \"user\" WHERE name='Open Data Canarias'"

   .. note:: Almacenar la API Key.

#. Editar la configuración de drupal y añadir la configuración a la base de datos:

   .. parsed-literal::

        sudo vim |drupalPath|/sites/default/settings.php

   Editar (lineas de la 213 a la 227), (linea 588):

   .. parsed-literal::

    $databases = array (
        'default' => array(
            'default' => array(
                'database' => '|mysqlDRUPALDatabase|',
                'username' => '|mysqlDRUPALUser|',
                'password' => '|mysqlDRUPALPassword|',
                'host' => 'localhost',
                'port' => '',
                'driver' => 'mysql',
                'prefix' => '',
            ),
        ),
    );

    $conf['ckan_sysadmin_api_key'] = fooAPIKey;

#. Habilitar ``mod_rewrite`` para usar clean URLs::

    sudo a2enmod rewrite

#. Editar el fichero de configuración de Apache para que procese ficheros PHP::

    sudo vim /etc/apache2/apache2.conf

   Añadir al final::

    AddType application/x-httpd-php .php

#. Crear VirtualHost:

   **Opción 1:** Copiar el fichero:

   .. parsed-literal::

        sudo cp |codePath|/VirtualHost.conf /etc/apache2/sites-available/odc2

   **Opción 2:** Crear a mano::

    sudo vim /etc/apache2/sites-available/odc2

   Copiar el siguiente texto:

   .. parsed-literal::

        <VirtualHost \*\:|portListen|>
            ServerName |serverName|

            #Drupal instance
            DocumentRoot |drupalPath|
            DirectoryIndex index.phtml index.html index.php index.htm

            WSGIScriptAlias |urlToCatalog| |ckanConfigPath|/apache.wsgi
            WSGIPassAuthorization On

            <Directory />
                # XXX Should this be deny? We get a "Client denied by server
                # configuration" without it
                allow from all
            </Directory>

            # Drupal has all request apart from |urlToCatalog|
            <Directory |drupalPath|>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
            </Directory>

        </VirtualHost>

#. Habilitar el VirtualHost ``odc2``::

    sudo a2ensite odc2

#. Añadir usuario y contraseña a tomcat::

    sudo vim /etc/tomcat7/tomcat-users.xml

   Añadir entre las etiquetas tomcat-users::

    <role rolename="manager-gui"/>
    <role rolename="admin-gui"/>
    <user username="fooTomcatUser" password="fooTomcatPassword" roles="manager-gui,admin-gui"/>

   .. note:: Almacenar el usuario y contraseña de tomcat

#. Editar el fichero server.xml para evitar conflictos con Apache::

    sudo vim /etc/tomcat7/server.xml

   Dejar así (linea 72 - 75)::

    <Connector port="8983" protocol="HTTP/1.1"
               connectionTimeout="20000"
               URIEncoding="UTF-8"
               redirectPort="8443" />

#. Crear en un nuevo contexto para solr:

   **Opción 1:** Copiar el fichero::

    sudo cp |codePath|/solr/solr.xml /etc/tomcat7/Catalina/localhost/solr.xml

   **Opción 2:** Crear a mano::

    sudo vim /etc/tomcat7/Catalina/localhost/solr.xml

   Editar así::

    <Context docBase="/usr/share/solr/solr-4.7.0.war" crossContext="true">
        <Environment name="solr/home" type="java.lang.String" value="/usr/share/solr" override="true" />
    </Context>

#. Crear la configuración para los cores:

   **Opción 1:** Copiar el fichero::

    sudo cp |codePath|/solr/solr.xml /usr/share/solr/solr.xml

   **Opción 2:** Crear a mano::

    sudo vim /usr/share/solr/solr.xml

   Editar así::

    <solr persistent="true" sharedLib="lib">
        <cores adminPath="/admin/cores">
            <core name ="ckan-schema-2.0" instanceDir="ckan">
                <property name="dataDir" value="/var/lib/solr/data/ckan" />
            </core>

            <core name ="drupal" instanceDir="drupal">
                <property name="dataDir" value="/var/lib/solr/data/drupal" />
            </core>
        </cores>
    </solr>

#. Configurar los logs de Solr.

   Editar la configuración de log4j::

    sudo vim /usr/share/tomcat7/lib/log4j.properties

   Editar así (linea 2)::

    solr.log=/var/log/solr

#. Añadir el log a logrotate::

    sudo vim /etc/logrotate.d/solr

   Editar así::

    /var/log/solr/solr.log {
        copytruncate
        daily
        rotate 5
        compress
        missingok
        create 640 tomcat7 tomcat7
    }

#. Editar el fichero de configuración de solr para drupal::

    sudo vim /etc/solr/drupal/conf/solrconfig.xml

   Comentar las siguientes lineas (71-72), (lineas 75 - 85), (linea 102) y remplazar (lineas 942 y 949)::

    <!--
    <lib dir="${solr.contrib.dir:../../../contrib}/extraction/lib" />
    <lib dir="${solr.contrib.dir:../../../contrib}/clustering/lib/" />
    -->

    <!--
    <lib dir="../../../contrib/extraction/lib" regex=".*\.jar" />
    <lib dir="../../../dist/" regex="solr-cell-\d.*\.jar" />

    <lib dir="../../../contrib/clustering/lib/" regex=".*\.jar" />
    <lib dir="../../../dist/" regex="solr-clustering-\d.*\.jar" />

    <lib dir="../../../contrib/langid/lib/" regex=".*\.jar" />
    <lib dir="../../../dist/" regex="solr-langid-\d.*\.jar" />

    <lib dir="../../../contrib/velocity/lib" regex=".*\.jar" />
    <lib dir="../../../dist/" regex="solr-velocity-\d.*\.jar" />
    -->

    <!-- <dataDir>${solr.data.dir:}</dataDir> -->

    solr.JsonUpdateRequestHandler ->  solr.UpdateRequestHandler
    solr.CSVRequestHandler -> solr.UpdateRequestHandler

#. Editar el fichero de configuración de solr para drupal::

    sudo vim /etc/solr/drupal/conf/solrconfig.xml

   Editar así (lineas 75 - 85), (102)::

    <!--
    <lib dir="../../../contrib/extraction/lib" regex=".*\.jar" />
    <lib dir="../../../dist/" regex="solr-cell-\d.*\.jar" />

    <lib dir="../../../contrib/clustering/lib/" regex=".*\.jar" />
    <lib dir="../../../dist/" regex="solr-clustering-\d.*\.jar" />

    <lib dir="../../../contrib/langid/lib/" regex=".*\.jar" />
    <lib dir="../../../dist/" regex="solr-langid-\d.*\.jar" />

    <lib dir="../../../contrib/velocity/lib" regex=".*\.jar" />
    <lib dir="../../../dist/" regex="solr-velocity-\d.*\.jar" />
    -->

    <!-- <dataDir>${solr.data.dir:}</dataDir> -->

#. Editar fichero de configuración:

   **Opción 1:** Copiar el fichero:

   .. parsed-literal::

    sudo cp |codePath|/ssmtp.conf  /etc/ssmtp/ssmtp.conf

   **Opción 2:** Crear a mano::

    sudo vim /etc/ssmtp/ssmtp.conf

   Dejar así::

    #
    # Config file for sSMTP sendmail
    #
    # The person who gets all mail for userids < 1000
    # Make this empty to disable rewriting.
    root=info@opendatacanarias.es

    # The place where the mail goes. The actual machine name is required no
    # MX records are consulted. Commonly mailhosts are named mail.domain.com
    mailhub=smtp.gmail.com:587

    # Where will the mail seem to come from?
    #rewriteDomain=

    # The full hostname
    hostname=info@opendatacanarias.es

    # Are users allowed to set their own From: address?
    # YES - Allow the user to specify their own From: address
    # NO - Use the system generated From: address
    #FromLineOverride=YES
    UseSTARTTLS=YES
    AuthUser=info@opendatacanarias.es
    AuthPass=fooPasswordEmail
    FromLineOverride=YES

   .. note:: Editar la contraseña del correo electrónico.

#. Añadir a los usuarios del sistema que se les va a permitir enviar emails.
   Editar el fichero /etc/ssmtp/revaliases::

     sudo vim /etc/ssmtp/revaliases

   Añadir al final::

    root:info@opendatacanarias.es:smtp.gmail.com:587
    fooUserServer:info@opendatacanarias.es:smtp.gmail.com:587

#. Configurar trabajo cron::

    crontab -e

   .. note:: Seleccionar editor

#. Añadir al final::

    @hourly /usr/lib/ckan/default/bin/paster --plugin=ckan tracking update -c /etc/ckan/default/production.ini && /usr/lib/ckan/bin/paster --plugin=ckan search-index rebuild -r -c /etc/ckan/default/production.ini

#. Reiniciar servicios::

    sudo service tomcat7 restart
    sudo service apache2 restart
    sudo service nginx restart

#. Listo