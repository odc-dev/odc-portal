=======================
Instalación paso a paso
=======================

En este modo se instala cada componente con independencia del resto. Esta modularidad es relativa debido a que algunos componentes depende de otros, al principio de cada uno se indican cuales son sus dependencias.

Pasos previos
-------------

#. Antes de instalar cada una de las partes es necesario actualizar e instalar algunos paquetes básicos::

    sudo apt-get update
    sudo apt-get dist-upgrade
    sudo apt-get install links2 vim git wget patch

#. Descargar código del portal desde el repositorio git::

    cd ~
    git clone https://bitbucket.org/odc-dev/odc-portal.git
    cd odc-portal
    git tag v0.1

Proceso
-------

El proceso se divide en la instalación de cada uno de los siguientes componentes:

.. toctree::
    :maxdepth: 1

    componentes/Apache
    componentes/MySQL
    componentes/PostgreSQL
    componentes/portalContenidos
    componentes/Tomcat
    componentes/Solr
    componentes/CKAN
    componentes/modulosExtras
    componentes/moduloODC
    componentes/crearAdministrador
    componentes/datasetPopulares
    componentes/parches
    componentes/drupalApacheSolr
    componentes/drupalAPIkeyCKAN
    componentes/drupalCleanURIs
    componentes/ServidorSMTP
    test