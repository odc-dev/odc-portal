Apache
------

Instalación
^^^^^^^^^^^
#. Se instala Apache y sus dependecias::

    sudo apt-get install apache2

#. Habilitar ``mod_rewrite`` para usar clean URLs::

    sudo a2enmod rewrite

#. Editar el fichero de configuración de Apache para que procese ficheros PHP::

    sudo vim /etc/apache2/apache2.conf

   Añadir al final::

    AddType application/x-httpd-php .php

#. Crear VirtualHost:

   **Opción 1:** Copiar el fichero:

   .. parsed-literal::

        sudo cp |codePath|/VirtualHost.conf /etc/apache2/sites-available/odc2

   **Opción 2:** Crear a mano::

    sudo vim /etc/apache2/sites-available/odc2

   Copiar el siguiente texto:

   .. parsed-literal::

        <VirtualHost \*\:|portListen|>
            ServerName |serverName|

            #Drupal instance
            DocumentRoot |drupalPath|
            DirectoryIndex index.phtml index.html index.php index.htm

            WSGIScriptAlias |urlToCatalog| |ckanConfigPath|/apache.wsgi
            WSGIPassAuthorization On

            <Directory />
                # XXX Should this be deny? We get a "Client denied by server
                # configuration" without it
                allow from all
            </Directory>

            # Drupal has all request apart from |urlToCatalog|
            <Directory |drupalPath|>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
            </Directory>

        </VirtualHost>

   .. parsed-literal:: Sustituir el ServerName por el dominio o ip del servidor.

#. Habilitar el VirtualHost ``odc2``::

    sudo a2ensite odc2

#. Reiniciar Apache:

   .. parsed-literal::
        |restartApache|

   .. note:: Si no está instalado CKAN este paso da error.