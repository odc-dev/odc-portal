Usuario de Administración
-------------------------

Dependencias
^^^^^^^^^^^^

La instalación del portal de contenidos esta condicionada a la instalación de los siguientes componentes:

* :doc:`CKAN`

.. note:: Se deben instalar las dependencias primero.

Crear Usuario
^^^^^^^^^^^^^

#. Activar el entorno virtual de CKAN para ejecutar comandos paster::

    . /usr/lib/ckan/default/bin/activate
    cd /usr/lib/ckan/default/src/ckan

#. Crear un usuario administrador para CKAN, llamado ``admin`` y asignarle contraseña::

    paster sysadmin add admin -c /etc/ckan/default/production.ini

   .. note:: Almacenar la contraseña del administrador.

#. Reiniciar Apache::

    sudo service apache2 restart