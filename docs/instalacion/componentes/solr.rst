Solr 4.7.0
----------

Dependencias
^^^^^^^^^^^^
La instalación del portal de contenidos esta condicionada a la instalación de los siguientes componentes:

* :doc:`Tomcat`

.. note:: Se deben instalar las dependencias primero.

Instalación
^^^^^^^^^^^

#. Descargar y descomprimir Solr::

    cd ~
    wget http://archive.apache.org/dist/lucene/solr/4.7.0/solr-4.7.0.tgz
    tar zxvf solr-4.7.0.tgz

#. Crear los directorios para los cores de Solr::

    sudo mkdir -p /usr/share/solr/ckan /var/lib/solr/data/ckan/index /etc/solr/ckan
    sudo mkdir -p /usr/share/solr/drupal /var/lib/solr/data/drupal/index /etc/solr/drupal

#. Copiar Solr a la ubicación donde se debe instalar::

    sudo cp solr-4.7.0/dist/solr-4.7.0.war /usr/share/solr

#. Copiar la configuración de ejemplo para cada uno de los cores::

    sudo cp -r solr-4.7.0/example/solr/collection1/conf /etc/solr/ckan
    sudo cp -r solr-4.7.0/example/solr/collection1/conf /etc/solr/drupal

#. Crear un directorio en la raiz de Solr y copiar allí los .jar::

    sudo mkdir /usr/share/solr/lib
    sudo cp solr-4.7.0/dist/* /usr/share/solr/lib
    sudo cp solr-4.7.0/contrib/analysis-extras/lib/* /usr/share/solr/lib
    sudo cp solr-4.7.0/contrib/clustering/lib/* /usr/share/solr/lib
    sudo cp solr-4.7.0/contrib/dataimporthandler/lib/* /usr/share/solr/lib
    sudo cp solr-4.7.0/contrib/extraction/lib/* /usr/share/solr/lib
    sudo cp solr-4.7.0/contrib/langid/lib/* /usr/share/solr/lib
    sudo cp solr-4.7.0/contrib/uima/lib/* /usr/share/solr/lib
    sudo cp solr-4.7.0/contrib/velocity/lib/* /usr/share/solr/lib
    sudo cp solr-4.7.0/dist/solrj-lib/* /usr/share/tomcat7/lib/
    sudo cp solr-4.7.0/example/resources/log4j.properties /usr/share/tomcat7/lib

#. Crear enlaces simbólicos entre configuraciones de /etc y /usr::

    sudo ln -s /etc/solr/ckan/conf /usr/share/solr/ckan/conf
    sudo ln -s /etc/solr/drupal/conf /usr/share/solr/drupal/conf

#. Crear el directorio para los logs::

        sudo mkdir /var/log/solr

Configuración
^^^^^^^^^^^^^
#. Comentar en los ficheros de configuración de los cores la inclusión de .jar externos, puesto que ya están todos incluídos::

    sudo vim /etc/solr/ckan/conf/solrconfig.xml
    sudo vim /etc/solr/drupal/conf/solrconfig.xml

   Editar así (lineas 75 - 85)::

    <!--
    <lib dir="../../../contrib/extraction/lib" regex=".*\.jar" />
    <lib dir="../../../dist/" regex="solr-cell-\d.*\.jar" />

    <lib dir="../../../contrib/clustering/lib/" regex=".*\.jar" />
    <lib dir="../../../dist/" regex="solr-clustering-\d.*\.jar" />

    <lib dir="../../../contrib/langid/lib/" regex=".*\.jar" />
    <lib dir="../../../dist/" regex="solr-langid-\d.*\.jar" />

    <lib dir="../../../contrib/velocity/lib" regex=".*\.jar" />
    <lib dir="../../../dist/" regex="solr-velocity-\d.*\.jar" />
    -->

   y (linea 102)::

    <!-- <dataDir>${solr.data.dir:}</dataDir> -->

#. Crear en un nuevo contexto para solr:

   **Opción 1:** Copiar el fichero:

   .. parsed-literal::

    sudo cp |codePath|/solr/solr.xml /etc/tomcat7/Catalina/localhost/solr.xml

   **Opción 2:** Crear a mano::

    sudo vim /etc/tomcat7/Catalina/localhost/solr.xml

   Editar así::

    <Context docBase="/usr/share/solr/solr-4.7.0.war" crossContext="true">
        <Environment name="solr/home" type="java.lang.String" value="/usr/share/solr" override="true" />
    </Context>

#. Crear la configuración para los cores:

   **Opción 1:** Copiar el fichero:

   .. parsed-literal::

    sudo cp |codePath|/solr/solr.xml /usr/share/solr/solr.xml

   **Opción 2:** Crear a mano::

    sudo vim /usr/share/solr/solr.xml

   Editar así::

    <solr persistent="true" sharedLib="lib">
        <cores adminPath="/admin/cores">
            <core name ="ckan-schema-2.0" instanceDir="ckan">
                <property name="dataDir" value="/var/lib/solr/data/ckan" />
            </core>

            <core name ="drupal" instanceDir="drupal">
                <property name="dataDir" value="/var/lib/solr/data/drupal" />
            </core>
        </cores>
    </solr>

#. Configurar los logs de Solr.

   Editar la configuración de log4j::

    sudo vim /usr/share/tomcat7/lib/log4j.properties

   Editar así (linea 2)::

    solr.log=/var/log/solr

#. Añadir el log a logrotate::

    sudo vim /etc/logrotate.d/solr

   Editar así::

    /var/log/solr/solr.log {
        copytruncate
        daily
        rotate 5
        compress
        missingok
        create 640 tomcat7 tomcat7
    }

#. Establecer permisos::

    sudo chown -R tomcat7:tomcat7 /var/log/solr /usr/share/solr /var/lib/solr

#. Reiniciar Tomcat::

        sudo service tomcat7 restart

#. Comprobar si Tomcat y SOLR están funcionando correctamente http://|serverName|:8983/solr