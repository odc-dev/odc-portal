Clean URIs - Drupal
-------------------

Dependencias
^^^^^^^^^^^^

* :doc:`portalContenidos`

Configuración
^^^^^^^^^^^^^

#. Acceder a la configuración de drupal como administrador en::

    opendatacanarias.es/?q=user

#. Acceder a la sección de Clean URIs en el menú::

    Configuración -> URL Limpias (Clean URIs)

#. Activar al opción  `Activar URL limpias`

#. Guardar y salir