API CKAN para Drupal
--------------------

Dependencias
^^^^^^^^^^^^

La instalación del portal de contenidos esta condicionada a la instalación de los siguientes componentes:

* :doc:`PostgreSQL`
* :doc:`portalContenidos`
* :doc:`CKAN`

.. note:: Se deben instalar las dependencias primero.

Configuración
^^^^^^^^^^^^^

#. Consultar la API Key en CKAN del usuario::

    sudo -u postgres -H -- psql -d ckan_default -c "SELECT apikey FROM \"user\" WHERE name='Open Data Canarias'"

   .. note:: Almacenar la API Key.

#. Editar la configuración de drupal (linea 588)::

    sudo vim /var/www/odc2-drupal/sites/default/settings.php

   Añadir::

    $conf['ckan_sysadmin_api_key'] = fooAPIKey;

#. Reiniciar Apache::

    sudo service apache2 restart
