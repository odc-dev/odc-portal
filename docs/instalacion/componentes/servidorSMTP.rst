Servidor de Correo
------------------

Instalación
^^^^^^^^^^^
#. Descargar e Instalar SMMTP::

    sudo apt-get install ssmtp

Configuración
^^^^^^^^^^^^^

#. Editar fichero de configuración:

   **Opción 1:** Copiar el fichero:

   .. parsed-literal::

    sudo cp |codePath|/ssmtp.conf  /etc/ssmtp/ssmtp.conf

   **Opción 2:** Crear a mano::

    sudo vim /etc/ssmtp/ssmtp.conf

   Dejar así::

    #
    # Config file for sSMTP sendmail
    #
    # The person who gets all mail for userids < 1000
    # Make this empty to disable rewriting.
    root=info@opendatacanarias.es

    # The place where the mail goes. The actual machine name is required no
    # MX records are consulted. Commonly mailhosts are named mail.domain.com
    mailhub=smtp.gmail.com:587

    # Where will the mail seem to come from?
    #rewriteDomain=

    # The full hostname
    hostname=info@opendatacanarias.es

    # Are users allowed to set their own From: address?
    # YES - Allow the user to specify their own From: address
    # NO - Use the system generated From: address
    #FromLineOverride=YES
    UseSTARTTLS=YES
    AuthUser=info@opendatacanarias.es
    AuthPass=fooPasswordEmail
    FromLineOverride=YES

   .. note:: Editar la contraseña del correo electrónico.

#. Añadir a los usuarios del sistema que se les va a permitir enviar emails.
   Editar el fichero /etc/ssmtp/revaliases::

     sudo vim /etc/ssmtp/revaliases

   Añadir al final::

    root:info@opendatacanarias.es:smtp.gmail.com:587
    fooUserServer:info@opendatacanarias.es:smtp.gmail.com:587

Comprobación
^^^^^^^^^^^^

#. Enviar un email de prueba para comprobar que funciona correctamente.
   Enviar por consola::

    sudo ssmtp info@opendatacanarias.es

   Teclear el mensaje y para terminar pulsar las teclas ``ctrl+d``

#. Comprobar que está en la bandeja de entrada.
