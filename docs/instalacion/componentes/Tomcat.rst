Tomcat
------
Instalación
^^^^^^^^^^^

#. Instalar tomcat y dependencias::

    sudo apt-get install openjdk-7-jre tomcat7 tomcat7-docs tomcat7-examples tomcat7-admin

Configuración
^^^^^^^^^^^^^

#. Parar tomcat::

    sudo service tomcat7 stop

#. Añadir usuario y contraseña a tomcat::

    sudo vim /etc/tomcat7/tomcat-users.xml

   Añadir entre las etiquetas tomcat-users::

    <role rolename="manager-gui"/>
    <role rolename="admin-gui"/>
    <user username="fooTomcatUser" password="fooTomcatPassword" roles="manager-gui,admin-gui"/>

   .. note:: Almacenar el usuario y contraseña de tomcat

#. Editar el fichero server.xml para evitar conflictos con Apache::

    sudo vim /etc/tomcat7/server.xml

   Dejar así (linea 72 - 75)::

    <Connector port="8983" protocol="HTTP/1.1"
               connectionTimeout="20000"
               URIEncoding="UTF-8"
               redirectPort="8443" />

   .. note::
    Comprobar si el puerto está libre:

    .. parsed-literal::

        netstat -aunt \|grep 8983

#. Reiniciar Tomcat::

    sudo service tomcat7 restart

#. Probar Tomcat en http://|serverName|:8983/manager