Modulo Open Data Canarias
-------------------------

Dependencias
^^^^^^^^^^^^

La instalación del portal de contenidos esta condicionada a la instalación de los siguientes componentes:

* :doc:`CKAN`
* :doc:`modulosExtras`

.. note:: Se deben instalar las dependencias primero.

Instalación
^^^^^^^^^^^

#. Copiar la extensión::

    sudo cp -r ~/odc-portal/ckan/ckanext-odc2 /usr/lib/ckan/default/src/

#. Instalación de la exntesión::

    sudo su
    . /usr/lib/ckan/default/bin/activate
    cd /usr/lib/ckan/default/src/ckanext-odc2/
    python setup.py develop
    exit

#. Editar fichero production.ini::

    sudo vim /etc/ckan/default/production.ini

   Descomentar los plugin (linea 86)::

    ckan.plugins = stats text_preview recline_preview datastore datapusher resource_proxy pdf_preview spatial_query spatial_metadata geojson_preview wms_preview odc2

   Descomentar la ruta del filestore (linea 125)::

    ckan.storage_path = /var/lib/ckan/default

#. Reiniciar Apache::

    sudo service apache2 restart