Portal de Contenidos
--------------------

Dependencias
^^^^^^^^^^^^

La instalación del portal de contenidos esta condicionada a la instalación de los siguientes componentes:

* :doc:`Apache`
* :doc:`MySQL`

.. note:: Se deben instalar las dependencias primero

Instalación
^^^^^^^^^^^
#. Se instalan las dependecias::

    sudo apt-get install php5 php5-mysql php5-gd

#. Copiar el código de drupal en la carpeta destino:

   .. parsed-literal::

        sudo cp -r |codePathDrupalCode| |drupalPath|

Configuración
^^^^^^^^^^^^^
#. Cambiar permisos para la carpeta pública:

   .. parsed-literal::

        sudo chown -R www-data:www-data |drupalPath|/sites/default/files

#. Editar la configuración de drupal y añadir la configuración a la base de datos:

   .. parsed-literal::

        sudo vim |drupalPath|/sites/default/settings.php

   Editar (lineas de la 213 a la 227):

   .. parsed-literal::

    $databases = array (
        'default' => array(
            'default' => array(
                'database' => '|mysqlDRUPALDatabase|',
                'username' => '|mysqlDRUPALUser|',
                'password' => '|mysqlDRUPALPassword|',
                'host' => 'localhost',
                'port' => '',
                'driver' => 'mysql',
                'prefix' => '',
            ),
        ),
    );

#. Preparar la Base de Datos para Drupal:

   .. parsed-literal::

        mysql -u root -p
        CREATE DATABASE |mysqlDRUPALDatabase|;

   Crear el usuario y otorgarle permisos:

   .. parsed-literal::

        CREATE USER '|mysqlDRUPALUser|'@'localhost' IDENTIFIED BY '|mysqlDRUPALPassword|';
        GRANT ALL PRIVILEGES ON |mysqlDRUPALDatabase|.* TO |mysqlDRUPALUser|\@localhost IDENTIFIED BY '|mysqlDRUPALPassword|';
        FLUSH PRIVILEGES;

   .. note:: Guardar los datos de conexión: usuario, contraseña y base de datos.

   Comprobar que el usuario está creado y salir de MySQL::

    SELECT * FROM mysql.user;
    exit

#. Restaurar la copia inicial de la base de datos:

   .. parsed-literal::

        mysql -u |mysqlDRUPALUser| -p |mysqlDRUPALDatabase| < |codePathDrupal|/|codeDrupalDump|

   .. note:: Solicita la contraseña para el usuario |mysqlDRUPALUser|

Comprobación
^^^^^^^^^^^^
Se puede comprobar accediendo a la url |urlToServerName|:

   .. note:: Es posible que solo se muestre el mensaje de bienvenida de Apache: "It Works!"

   .. note:: El administrador del sitio para drupal es usuario y contraseña: admin. Se debe cambiar.