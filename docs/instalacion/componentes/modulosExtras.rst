Plugins extras de CKAN
----------------------

Dependencias
^^^^^^^^^^^^

La instalación del portal de contenidos esta condicionada a la instalación de los siguientes componentes:

* :doc:`PostgreSQL`
* :doc:`CKAN`
* :doc:`moduloODC` (se debe realizar después de instalar esta sección)

.. note:: Hasta que no se descomenten los plugins en la configuración de CKAN no funcionan (dependencia :doc:`moduloODC`).

Datastore
^^^^^^^^^

#. Crear un usuario para la base de datos del datastore::

    sudo -u postgres createuser -S -D -R -P -l datastore_default

   .. note:: Almacenar la contraseña.

#. Crear la base de datos de para el datastore, el dueño debe ser el usuario creado previamente::

    sudo -u postgres createdb -O ckan_default datastore_default -E utf-8

#. Editar fichero production.ini::

    sudo vim /etc/ckan/default/production.ini

   Editar contraseñas (linea 48-49)::

    ckan.datastore.write_url = postgresql://ckan_default:fooCkanPassword@localhost/datastore_default
    ckan.datastore.read_url = postgresql://datastore_default:fooDatastorePassword@localhost/datastore_default

#. Activar el entorno virtual de CKAN::

    . /usr/lib/ckan/default/bin/activate

#. Establecer en la base de datos los permisos especificados en el fichero de configuración::

    paster --plugin=ckan datastore set-permissions postgres -c /etc/ckan/default/production.ini

   .. note:: Solicita contraseña para el usuario de la máquina.

Filestore
^^^^^^^^^

#. Crear el directorio donde CKAN guardará los ficheros subidos::

    sudo mkdir -p /var/lib/ckan/default

#. Establecer los permisos necesarios para que Apache pueda escribir en la ruta::

    sudo chown www-data /var/lib/ckan/default
    sudo chmod u+rwx /var/lib/ckan/default

Spatial
^^^^^^^

#. Instalar otras dependencias de la extensión::

    sudo apt-get install libxml2-dev libxslt1-dev libgeos-c1

#. Crear en la base de datos las tablas y funciones necesarias y poblarlas::

    sudo -u postgres psql -d ckan_default -f /usr/share/postgresql/9.1/contrib/postgis-1.5/postgis.sql
    sudo -u postgres psql -d ckan_default -f /usr/share/postgresql/9.1/contrib/postgis-1.5/spatial_ref_sys.sql

#. cambiar el dueño de las tablas espaciales al usuario de CKAN (ckan_default)::

    sudo -u postgres psql postgres

   Ejecutar en postgres::

    \c ckan_default
    ALTER TABLE spatial_ref_sys OWNER TO ckan_default;
    ALTER TABLE geometry_columns OWNER TO ckan_default;
    \q

#. activar el entorno virtual de CKAN para ejecutar comandos paster::

    sudo su
    . /usr/lib/ckan/default/bin/activate
    cd /usr/lib/ckan/default/src/ckan

#. Instalar la extensión::

    pip install -e git+https://github.com/okfn/ckanext-spatial.git@stable#egg=ckanext-spatial

#. En el directorio de la extensión instalar los módulos de python que se requieren::

    cd /usr/lib/ckan/default/src/ckanext-spatial/
    pip install -r pip-requirements.txt
    exit
