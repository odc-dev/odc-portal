Parches
-------

Dependencias
^^^^^^^^^^^^

La instalación del portal de contenidos esta condicionada a la instalación de los siguientes componentes:

* :doc:`CKAN`

.. note:: Se deben instalar las dependencias primero.

Aplicar parches
^^^^^^^^^^^^^^^
#. Ejecutar patch con cada parche::

    sudo patch -p1 < ~/odc-portal/ckan/patches/traducir_widget_1.patch
    sudo patch -p1 < ~/odc-portal/ckan/patches/traducir_widget_2.patch
    sudo patch -p1 < ~/odc-portal/ckan/patches/redirigir_usuario_home.patch
    sudo patch -p1 < ~/odc-portal/ckan/patches/24_items_per_page.patch
    sudo patch -p1 < ~/odc-portal/ckan/patches/traducir_dataset_extent_1.patch
    sudo patch -p1 < ~/odc-portal/ckan/patches/traducir_dataset_extent_2.patch
