MySQL
-----

Instalación
^^^^^^^^^^^
#. Se instala MySQL y sus dependecias::

    sudo apt-get install mysql-server

   Añadir contraseña para el root para la base de datos:

   .. note:: Añadir y guardar la contraseña root a MySQL

    .. parsed-literal::

        Password\: |mysqlROOTPassword|