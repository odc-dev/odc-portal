CKAN
----

Dependencias
^^^^^^^^^^^^

La instalación del portal de contenidos esta condicionada a la instalación de los siguientes componentes:

* :doc:`Apache`
* :doc:`PostgreSQL`
* :doc:`Solr`

.. note:: Se deben instalar las dependencias primero.

Instalación
^^^^^^^^^^^

#. Instalar dependencias de CKAN::

        sudo apt-get install -y nginx libapache2-mod-wsgi libpq5 python python-dev

#. Descargar la distribución de CKAN::

        cd ~
        wget http://packaging.ckan.org/python-ckan_2.2_amd64.deb

#. Instalar la distribución::

        sudo dpkg -i python-ckan_2.2_amd64.deb

#. Copiar el fichero de configuración::

    sudo cp ~/odc-portal/ckan/etc/production.ini /etc/ckan/default/production.ini

#. Copiar el esquema por defecto del core de CKAN y enlazar al esquema de CKAN (Solr)::

    sudo rm -f /etc/solr/ckan/conf/schema.xml
    sudo ln -s /usr/lib/ckan/default/src/ckan/ckan/config/solr/schema-2.0.xml /etc/solr/ckan/conf/schema.xml

#. Crear usuarios de la base de datos::

    sudo -u postgres createuser -S -D -R -P ckan_default

   .. note::
        Guardar los datos del usuario.

#. Crear bases de datos::

    sudo -u postgres createdb -O ckan_default ckan_default -E utf-8

#. Editar fichero de configuración::

    sudo vim /etc/ckan/default/production.ini

   Editar los datos conexión del usuario de ckan (linea 46)::

    sqlalchemy.url = postgresql://ckan_default:fooCkanPassword@localhost/ckan_default

#. Inicializar la base de datos::

    sudo ckan db init

#. Reiniciar Apache y nginx::

    sudo service apache2 restart
    sudo service nginx restart

#. Comprobar que está instalado en http://|serverName|/datos

.. note:: No funciona correctamente hasta que todo esté instalado.