Datasets Populares
------------------

Dependencias
^^^^^^^^^^^^

La instalación del portal de contenidos esta condicionada a la instalación de los siguientes componentes:

* :doc:`CKAN`

.. note:: Se deben instalar las dependencias primero.

Configuración
^^^^^^^^^^^^^

#. Configurar trabajo cron::

    crontab -e

   .. note:: Seleccionar editor

#. Añadir al final::

    @hourly /usr/lib/ckan/default/bin/paster --plugin=ckan tracking update -c /etc/ckan/default/production.ini && /usr/lib/ckan/bin/paster --plugin=ckan search-index rebuild -r -c /etc/ckan/default/production.ini

