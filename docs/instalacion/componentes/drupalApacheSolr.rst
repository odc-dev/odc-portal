Apache Solr Search para Drupal
------------------------------

Dependencias
^^^^^^^^^^^^

La instalación del portal de contenidos esta condicionada a la instalación de los siguientes componentes:

* :doc:`Solr`
* :doc:`portalContenidos`

.. note:: Se deben instalar las dependencias primero.

Configuración
^^^^^^^^^^^^^

#. Copiar los archivos del esquema de Drupal al core de SOLR::

    sudo cp /var/www/odc2-drupal/sites/all/modules/apachesolr/solr-conf/solr-4.x/* /etc/solr/drupal/conf/

#. Sustituir algunas funciones de los ficheros de configuración de ejemplo que ya están "deprecated" en las últimas versiones de SOLR::

    sudo vim /etc/solr/drupal/conf/solrconfig.xml

   Remplazar (lineas 942 y 949)::

    solr.JsonUpdateRequestHandler ->  solr.UpdateRequestHandler
    solr.CSVRequestHandler -> solr.UpdateRequestHandler

#. Comentar en el fichero de configuración la inclusión de .jar externos::

    sudo vim /etc/solr/drupal/conf/solrconfig.xml

   Comentar las siguientes lineas (71-72)::

    <!--
    <lib dir="${solr.contrib.dir:../../../contrib}/extraction/lib" />
    <lib dir="${solr.contrib.dir:../../../contrib}/clustering/lib/" />
    -->

#. Reiniciar Tomcat::

    sudo service tomcat7 restart
