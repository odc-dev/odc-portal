============
Introducción
============
¡Bienvenido a la documentación del portal Open Data Canarias!

Aquí se encuentra toda la información sobre las tareas de administración sobre el portal.

:doc:`instalacion/index`
    Instalación del portal desde cero. Deja el estado del portal en un estado inicial, listo para funcionar pero sin contenido alguno.

:doc:`mantenimiento/actualizaciones`
    Establece los pasos a realizar para llevar a cabo una actualización en el portal.

:doc:`mantenimiento/backup/index`
    Establece los pasos para gestionar las copias de seguridad de todo el portal tanto crear como recuperar.

:doc:`changelog`
    Descripción de los cambios realizados en el portal.