==========
Contribuir
==========

Si encuentra alguna errata en la documentación o en alguna parte del código remita  una `incidencia <https://bitbucket.org/odc-dev/odc-portal/issues>`_ o enviar un correo electronico a info@opendatacanarias.es

¡Gracias!